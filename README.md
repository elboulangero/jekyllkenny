Jekyllkenny
===========

Jekyllkenny is a simple bash script to ease your daily jekyll blogging. It
simply automates some of the everyday blogging commands.

Its main functions are:

- creating a draft
- publishing a draft
- building and serving locally
- building and syncing with a remote server

There's actually no need to install Jekyll on your machine, but there's a need
for Docker. All the Jekyll work is done inside a container. For more details,
please visit <https://github.com/jekyll/docker>.

When you sync with a remote server, only the static site is sent to the server.
In other words, there's no need to install Jekyll on the server, a web server
is enough.

As for the source of your blog, Jekyllkenny does nothing about it. It's up to
you to setup your `.gitignore`, and commit and push whenever you feel like.



Install
-------

Type the following command as root:

	install -m0755 jekyllkenny /usr/local/bin



Get started
-----------

Navigate to the root of your Jekyll blog and type:

	jekyllkenny init

Then fill the file `.jekyllkennyrc` that was just created with your favorite text editor.

	vi .jekyllkennyrc

For more info on the available commands:

	jekyllkenny help



Publishing workflow
-------------------

Post publishing is done using symlinks. A draft is first copied with its
original name in the post directory, then a symlink with the right name
is added. It allows you to see your posts sorted by name (and not by date)
in the post directory.

This is my own personal twist to the common jekyll workflow.
